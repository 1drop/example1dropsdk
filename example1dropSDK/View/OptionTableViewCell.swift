//
//  OptionTableViewCell.swift
//  example1dropSDK
//
//  Created by 허문용 on 2020/11/23.
//

import UIKit

class OptionTableViewCell : UITableViewCell, UITextFieldDelegate{
    var actionSwitchContensStatus : (() -> Void)?
    var actionBtnSetting : (() -> Void)?
    var txtFieldStatus : (() -> Void)?
    @IBOutlet weak var labelContentName : UILabel!
    @IBOutlet weak var switchContentStatus: UISwitch!
    @IBOutlet weak var txtFieldValue: UITextField!
    @IBOutlet weak var btnChangeSet: UIButton!
    
    @IBAction func switchActionContentsStatus(_ sender: UISwitch) {
        actionSwitchContensStatus?()
    }
    @IBAction func btnActionChangeSet(_ sender: UIButton) {
        actionBtnSetting?()
    }
    @IBAction func txtFieldAction(_ sender: UITextField) {
        txtFieldStatus?()
    }
}
