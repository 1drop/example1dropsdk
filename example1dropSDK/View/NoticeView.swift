//
//  NoticeView.swift
//  example1dropSDK
//
//  Created by 허문용 on 2020/11/24.
//

import UIKit
class NoticeView : UIView {
    let xibName = "NoticeView"
    @IBOutlet weak var noticeDialog: UIView!
    @IBOutlet weak var noticeView: UIView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var txtViewExplain: UITextView!
    @IBOutlet weak var btnOk: UIButton!
//    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var txtFieldFolderString: UITextField!
    
    var btnOkAction : (() -> Void)?
//    var btnCancelAction : (() -> Void)?
    var txtFieldClosure : (() -> Void)?
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInit()
    }
    
    func commonInit(){
        Bundle.main.loadNibNamed(xibName, owner: self, options: nil)
        addSubview(noticeDialog)
        noticeDialog.frame = self.bounds
        noticeDialog.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        noticeView.layer.cornerRadius = 10
        txtViewExplain.isEditable = false
        btnOk.frame.size.width = 300
    }
    
    func setCustomView(){
        txtFieldFolderString.isHidden = false
        
    }
    
    func setTwoButton(){
        txtFieldFolderString.isHidden = false
//        btnCancel.isHidden = false
        btnOk.frame.size.width = 150
    }
    
    func setTitleContents(title : String, contents : String){
        labelTitle.text = title
        txtViewExplain.text = contents
    }
    
    func showDialog(superView : UIViewController){
        UIView.transition(with: superView.view, duration: 0, options: [], animations: {
            let keyWindow = UIApplication.shared.windows.first { $0.isKeyWindow }
            self.frame = keyWindow!.bounds
            keyWindow!.addSubview(self)
        }, completion: nil)

    }
    
    func hideDialog(superView : UIViewController) {
        UIView.transition(with: superView.view, duration: 0, options: [], animations: {
          self.removeFromSuperview()
        }, completion: nil)
    }
    @IBAction func btnActionOk(_ sender: UIButton) {
        btnOkAction?()
    }
//    @IBAction func btnActionCancel(_ sender: UIButton) {
//        btnCancelAction?()
//    }
    
    @IBAction func callWhenTxtFieldEnded(_ sender: UITextField) {
        txtFieldClosure?()
    }
}
