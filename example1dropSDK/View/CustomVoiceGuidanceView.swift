//
//  CustomVoiceGuidanceView.swift
//  example1dropSDK
//
//  Created by 허문용 on 2020/12/02.
//

import UIKit
import Framework1drop
class CustomVoiceGuidanceView : UIView, UITextFieldDelegate {
    let xibName = "CustomVoiceGuidanceView"
    @IBOutlet weak var customVoiceGuidanceDialog: UIView!
    @IBOutlet weak var customVoiceGuidanceView: UIView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var txtFieldLangCode: UITextField!
    
    @IBOutlet weak var customVgtableView: UITableView!
    @IBAction func btnActionOk(_ sender: UIButton) {
        langAndLocale = txtFieldLangCode.text ?? ""
        btnOkActionClosure?()
    }
    @IBAction func btnActionCancel(_ sender: UIButton) {
        btnCancelActionClosure?()
    }
    var btnOkActionClosure : (() -> Void)?
    var btnCancelActionClosure : (() -> Void)?
    var tableViewValue : (originY : CGFloat, yToModify : CGFloat)!
    private var customVgStrArr = Array<String>(repeating: "", count: VoiceGuidanceIndex.allCases.count - 1)
    private var langAndLocale : String = ""
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInit()
    }
    
    private func commonInit(){
        Bundle.main.loadNibNamed(xibName, owner: self, options: nil)
        addSubview(customVoiceGuidanceDialog)
        customVoiceGuidanceDialog.frame = self.bounds
        customVoiceGuidanceDialog.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        customVoiceGuidanceView.layer.cornerRadius = 10
        self.customVgtableView.delegate = self
        self.customVgtableView.dataSource = self
        customVgtableView.delaysContentTouches = false
        let xibTableView = UINib(nibName: "CustomVoiceGuidanceCell", bundle: nil)
        self.customVgtableView.register(xibTableView, forCellReuseIdentifier: "vgCell")
        tableViewValue = (customVgtableView.frame.origin.y, 0)
    }
    
    func setTxtFieldAndTxtViewAtTableView(customVgArr : [String], langAndLocale : String){
        self.customVgStrArr = customVgArr
        self.langAndLocale = langAndLocale
        txtFieldLangCode.text = self.langAndLocale
        
    }
    
    func getTxtFieldAndTxtViewAtTableView() -> (customVgArr : [String], langAndLocale : String){
        return (self.customVgStrArr, self.langAndLocale)
    }
    
    func showDialog(superView : UIViewController){
        registerForKeyboardNotifications()
        UIView.transition(with: superView.view, duration: 0, options: [], animations: {
            let keyWindow = UIApplication.shared.windows.first { $0.isKeyWindow }
            self.frame = keyWindow!.bounds
            keyWindow!.addSubview(self)
        }, completion: nil)

    }
    
    func hideDialog(superView : UIViewController) {
        NotificationCenter.default.removeObserver(self)
        UIView.transition(with: superView.view, duration: 0, options: [], animations: {
          self.removeFromSuperview()
        }, completion: nil)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.langAndLocale = textField.text ?? "unknown"
    }
}
extension CustomVoiceGuidanceView : UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return VoiceGuidanceIndex.allCases.count - 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "vgCell", for: indexPath) as! CustomVoiceGuidanceCell

        switch indexPath.row {
        case VoiceGuidanceIndex.waitAMoment.rawValue:
            cell.labelContentTitle.text = "CustomVoiceGuidanceView0".localized
            break
        case VoiceGuidanceIndex.howToUseAtFirst.rawValue:
            cell.labelContentTitle.text = "CustomVoiceGuidanceView1".localized
            break
        case VoiceGuidanceIndex.waitForBloodDrop.rawValue:
            cell.labelContentTitle.text = "CustomVoiceGuidanceView2".localized
            break
        case VoiceGuidanceIndex.bloodDropped.rawValue:
            cell.labelContentTitle.text = "CustomVoiceGuidanceView3".localized
            break
        case VoiceGuidanceIndex.notifyLeftTime.rawValue:
            cell.labelContentTitle.text = "CustomVoiceGuidanceView4".localized
            break
        case VoiceGuidanceIndex.result.rawValue:
            cell.labelContentTitle.text = "CustomVoiceGuidanceView5".localized
            break
        case VoiceGuidanceIndex.higher.rawValue:
            cell.labelContentTitle.text = "CustomVoiceGuidanceView6".localized
            break
        case VoiceGuidanceIndex.lower.rawValue:
            cell.labelContentTitle.text = "CustomVoiceGuidanceView7".localized
            break
        default:
            break
        }
        cell.txtViewContent.text = customVgStrArr[indexPath.row]
        cell.delegateClosure = {
            self.customVgStrArr[indexPath.row] = cell.txtViewContent.text ?? ""
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //선택된 로우(셀) 처리
        tableView.deselectRow(at: indexPath, animated: true) //선택해
        return
    }
    
    func registerForKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification , object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        guard let keyboardFrame = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }
        if tableViewValue.yToModify == 0{
            customVgtableView.frame.origin.y -= (keyboardFrame.height / 2)
            tableViewValue.yToModify = customVgtableView.frame.origin.y
        }
        else {
            customVgtableView.frame.origin.y = tableViewValue.yToModify
        }
        customVgtableView.isScrollEnabled = false
    }
    @objc func keyboardWillHide(_ notification: Notification) {
        customVgtableView.frame.origin.y = tableViewValue.originY
        customVgtableView.isScrollEnabled = true
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        DispatchQueue.main.async {
            self.customVgtableView.reloadData()
        }
        self.endEditing(true)
    }

}
