//
//  CustomVoiceGuidanceCell.swift
//  example1dropSDK
//
//  Created by 허문용 on 2020/12/03.
//

import UIKit

class CustomVoiceGuidanceCell : UITableViewCell, UITextViewDelegate{
    @IBOutlet weak var labelContentTitle: UILabel!
    @IBOutlet weak var txtViewContent: UITextView!
    @IBOutlet weak var vgCellView: UIView!

    var delegateClosure : (() -> Void)?
    private let xibName = "CustomVoiceGuidanceCell"
    override func awakeFromNib() {
        super.awakeFromNib()
        self.txtViewContent.delegate = self
    }

    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        if textView == txtViewContent{
            delegateClosure?()
        }
        return true
    }
}
