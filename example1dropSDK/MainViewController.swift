//
//  ViewController.swift
//  example1dropSDK
//
//  Created by 허문용 on 2020/11/23.
//

import UIKit
import AVFoundation
import MediaPlayer
import Framework1drop

enum OptionSet : Int {
    case vgOnOff = 0
    case vgVolume = 1
    case effectOnOff = 2
    case effectVolume = 3
    case checkSdkVer = 4
    case changeUnit = 5
    
}
enum SupplySet : Int {
    case updateResource = 0
    case customVoiceGuidance = 1
    case writeLog = 2
}

class MainViewController: UIViewController {
    @IBOutlet weak var labelCameraAccess: UILabel!
    @IBOutlet weak var labelOsVersion: UILabel!
    @IBOutlet weak var labelSdkVersion: UILabel!
    
    @IBOutlet weak var btnMeasure: UIButton!
    @IBOutlet weak var btnStep1: UIButton!
    @IBOutlet weak var btnStep2: UIButton!
    @IBOutlet weak var btnStep3: UIButton!
    
    @IBOutlet weak var optionView: UIView!
    @IBOutlet weak var optionTableView: UITableView!
    
    
    let MainToQrReaderSegue : String = "MainToQrReaderSegue"
    let MainToMeasureSegue : String = "MainToMeasureSegue"
    let MainToQrReadStepSegue : String = "MainToQrReadStepSegue"
    let MainToMeasureStepSegue : String = "MainToMeasureStepSegue"
    
    let availableMeasure = AvailableMeasure()
    let dataManager = DataManager()
    let cellList = [["mainVCAudioGuidanceSwitch".localized, "mainVCAudioGuidanceVolume".localized, "mainVCSoundEffectSwitch".localized, "mainVCSoundEffectVolume".localized], ["mainVCResourceFileUpdate".localized, "mainVCAudioGuidanceCustom".localized, "mainVCWriteLogFiles".localized]]
    
    let VgDefaultValue : Float = 0.3
    let EffectDefaultValue : Float = 1.0
    var cameraAcessStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
    var isAbleToMeasure : (step1 : Bool, step2 : Bool, step3 : Bool) = (false, false, false)
    var qrStringValue : String = ""
    
    var verifiedQr : (hasError : Bool, errCode : String)?
    var logFolderName : String? = nil
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == MainToQrReaderSegue {
            let vcDestination = segue.destination as! QrReaderViewController
            vcDestination.modalPresentationStyle = .fullScreen
            vcDestination.modalTransitionStyle = .crossDissolve
        }
        else if segue.identifier == MainToMeasureSegue{
            let vcDestination = segue.destination as! MeasureViewController
            vcDestination.modalPresentationStyle = .fullScreen
            vcDestination.modalTransitionStyle = .crossDissolve
        }
        else if segue.identifier == MainToQrReadStepSegue{
            let vcDestination = segue.destination as! QrReadStepViewController
            vcDestination.modalPresentationStyle = .fullScreen
            vcDestination.modalTransitionStyle = .crossDissolve
        }
    }
    
    var strForCustomVg = ["mainVCPleaseWait".localized, "mainVCPrepareMeasure".localized, "mainVCApplyBlood".localized, "mainVCMeasuringStart".localized,"mainVCSecondLeft".localized, "mainVCResultValue".localized, "mainVCResultValueHigh".localized, "mainVCResultValueLow".localized]
    var languageAndLocale = "ko-KR"
    var optionViewValue : (originY : CGFloat, yToModify : CGFloat)!
    override func viewDidLoad() {
        super.viewDidLoad()

        initData()
        initLayout()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        optionViewValue = (optionView.frame.origin.y, 0)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
/// ------------------------------------------------------------------------------------------
    /// framework1drop - Function
    /// ------------------------------------------------------------------------------------------
    /// Declaration - func setCustomGuidance(customStringArray: [String], langAndLocale : String)
    /// ReturnType - (hasError : Bool, errCode : DataError?)
    /// Discussion - You can customize audio guidance. Complete sequence by entering applicable sentences for each case.
    ///     This method includes validation test. If ReturnType of hasError is false, CustomSet is completed.
    ///     See example below for the parameter of customStringArray.
    ///       Array[0] = “Please wait for a moment.”    //Message to ask user to wait for next step
    ///       Array[1] = “After inserting cartridge, put your phone down so that the back of your phone is facing up.”    //Message to explain tasks to be completed before measuring.
    ///       Array[2] =“Apply blood into the cartridge. Do not lift your finger until you hear a notification sound.”    //Message to prompt application of blood
    ///       Array[3] = “Measuring started.”    //Message to show successful application of blood
    ///       Array[4] = “%@ seconds left.”    //Message to show remaining time
    ///       Array[5] = ”%@ result value is %@ %@ Per %@ .”    //Message to show results
    ///       Array[6] = "Your results are lower than normal"
    ///       Array[7] = "Your results are higher than normal"
    ///     Refer to the  ‘ISO Language Code Table' for the parameter of langAndLocale. If not applicable, the validation test will fail.
    /// Attention/Tip - Be mindful of content with %@. For detailed instructions, refer to 1drop API Reference.
    /// ------------------------------------------------------------------------------------------
    /// Link - https://www.notion.so/setCustomGuidance-str-String-langAndLocale-String-8fc2f829353a423b8cc60f0e6e657ee0
/// ------------------------------------------------------------------------------------------
    func setCustomVoiceGuidance(){
        let customStatus = MeasuringManager.shared.setCustomGuidance(customStringArray: strForCustomVg, langAndLocale: languageAndLocale)
        if !customStatus.hasError{
            showNoticeView(title: "mainVCSuccess".localized, contents: "mainVCAudioGuidanceHasChanged".localized)
        }
        else {
            showNoticeView(title: "mainVCFailed".localized, contents: "mainVCFailedToChangeAudioGuidance".localized + "\(customStatus.errCode)")
        }
    }
    func initData(){
/// ------------------------------------------------------------------------------------------
    /// framework1drop - Variable
    /// ------------------------------------------------------------------------------------------
    /// Declaration - var isEnabledEffectSound: Float { get set }
    /// Discussion - You can choose to enable/disable sound effects during measuring.
    ///         Default value is true.
    ///         When measuring is completed, all values return to default values.
    /// Attention/Tip - Because it is difficult to access the smartphone screen during measuring, enable sound effects to check progress.
    /// ------------------------------------------------------------------------------------------
    /// Link - https://www.notion.so/isEnabledEffectSound-bf971433c8c740018ddae37115f5fd5c
/// ------------------------------------------------------------------------------------------
    /// framework1drop - Variable
    /// ------------------------------------------------------------------------------------------
    /// Declaration - var isEnabledGuideSound: Float { get set }
    /// Discussion - You can choose to enable/disable audio guidance during measuring.
    ///         Default value is true.
    ///         When measuring is completed, all values return to default values.
    /// Attention/Tip - Because it is difficult to access the smartphone screen during measuring, audio guidance is recommended.
    /// ------------------------------------------------------------------------------------------
    /// Link - https://www.notion.so/isEnabledGuideSound-9d54b06bd23f40278ce3eaf8a56601de
/// ------------------------------------------------------------------------------------------
    /// framework1drop - Variable
    /// ------------------------------------------------------------------------------------------
    /// Declaration - var isLogRecording: Float { get set }
    /// Discussion - The log that is used to display result value is saved in the topmost folder.
    ///         Default value is false.
    ///         When measuring is completed, all values return to default values.
    /// Attention/Tip - If there are issues with measuring or results, save log file and send to 1drop for further analysis.
    /// ------------------------------------------------------------------------------------------
    /// Link - https://www.notion.so/isLogRecording-bc08d39c3362487b918f4d3a469a78bc
/// ------------------------------------------------------------------------------------------
        MeasuringManager.shared.isEnabledEffectSound = true
        MeasuringManager.shared.isEnabledGuideSound = true
        
/// ------------------------------------------------------------------------------------------
    /// framework1drop - Variable
    /// ------------------------------------------------------------------------------------------
    /// Declaration - var guideSoundVolume: Float { get set }
    /// Discussion - You can control the volume of audio guidance and check settings.
    ///     Default value is 0.4, and the setting range is 0.1 ~ 1.0.
    ///     When measuring is completed, all values return to default values.
    /// Attention/Tip - Audio guidance may sound louder than the sound effects. Adjust volume as user sees fit.
    /// ------------------------------------------------------------------------------------------
    /// Link - https://www.notion.so/guideSoundVolume-d5aeb7753e3b444b8719c0e67fe75b18
/// ------------------------------------------------------------------------------------------
    /// framework1drop - Variable
    /// ------------------------------------------------------------------------------------------
    /// Declaration - var effectSoundVolume: Float { get set }
    /// Discussion - You can set the size of the sound effect that occurs during the measurement process, or you can obtain the setting status.
    ///     Default value is 1.0, and the setting range is 0.1 ~ 1.0.
    ///     When measuring is completed, all values return to default values.
    /// Attention/Tip - Temporarily adjust the instrument volume before measurement starts. When the measurement is finished, it is set to the original instrument volume size.
    /// ------------------------------------------------------------------------------------------
    /// Link - https://www.notion.so/effectSoundVolume-59baba512ce54b51a1fe8525291a49a0
/// ------------------------------------------------------------------------------------------
        MeasuringManager.shared.guideSoundVolume = VgDefaultValue
        MeasuringManager.shared.effectSoundVolume = EffectDefaultValue
        
        MeasuringManager.shared.isLogRecording = false
        isAbleToMeasure = (false, false, false)
    }
    func initLayout(){
        registerForKeyboardNotifications()
        labelSdkVersion.text = "ver " + availableMeasure.getCurrentVersion()
        labelCameraAccess.text = "mainVCCameraCAccess".localized
        labelOsVersion.text = "mainVCOSVersionCheck".localized

        checkCameraAccess()
        if cameraAcessStatus != .notDetermined{
            checkSystemVersion()
        }
        optionTableView.delegate = self
        optionTableView.dataSource = self
        let xibOptionTableView = UINib(nibName: "OptionTableViewCell", bundle: nil)
        optionTableView.register(xibOptionTableView, forCellReuseIdentifier: "OptionCell")

        btnMeasure.setTitle("mainVCStartMeasuring".localized, for: .normal)
        
        btnStep1.setTitle("mainVCActionNeeded".localized, for: .normal)
        btnStep1.setTitleColor(UIColor.systemRed, for: .normal)
        if qrStringValue == ""{
            btnStep2.setTitle("mainVCActionNeeded".localized, for: .normal)
            btnStep2.setTitleColor(UIColor.systemRed, for: .normal)
        }
        else{
            btnStep2.setTitle("mainVCActionCompleted".localized, for: .normal)
            btnStep2.setTitleColor(UIColor.systemBlue, for: .normal)
        }
        
        btnStep3.setTitle("mainVCActionNeeded".localized, for: .normal)
        btnStep3.setTitleColor(UIColor.systemRed, for: .normal)
    }
    
    func reloadState(){
        registerForKeyboardNotifications()
        if isAbleToMeasure.step1{
            btnStep1.setTitle("mainVCActionCompleted".localized, for: .normal)
            btnStep1.setTitleColor(UIColor.systemBlue, for: .normal)
        }
        else {
            btnStep1.setTitle("mainVCActionNeeded".localized, for: .normal)
            btnStep1.setTitleColor(UIColor.systemRed, for: .normal)
        }
        
        if qrStringValue != "" {
/// ------------------------------------------------------------------------------------------
    /// framework1drop - Function
    /// ------------------------------------------------------------------------------------------
    /// Declaration - func setStringQRCode(code : String)
    /// ReturnType - (hasError: String, errCode: String)
    /// Discussion - String from the decoded QR is entered as parameter to prepare for measuring.
    ///     This method includes validation test. If ReturnType of hasError is false, measuring will progress.
    ///     If hasError is true, please refer to the Error Codes Table to identify the returned errCode.
    /// Attention/Tip - This function is required during the measuring process. Please be mindful when using.
    /// ------------------------------------------------------------------------------------------
    /// Link - https://www.notion.so/setStringQRCode-String-b8f8b2c7095845819f57eb6587f2c807
/// ------------------------------------------------------------------------------------------
            verifiedQr = dataManager.setStringQRCode(code: qrStringValue)
            if !(verifiedQr?.hasError ?? true) {
                isAbleToMeasure.step2 = true
                btnStep2.setTitle("mainVCActionCompleted".localized, for: .normal)
                btnStep2.setTitleColor(UIColor.systemBlue, for: .normal)
                showNoticeView(title: "mainVCActionCompleted".localized, contents: "mainVCCorrectQRCodeConfirm".localized)
            }
            else {
                isAbleToMeasure.step2 = false
                btnStep2.setTitle("mainVCFailed".localized + "(\(verifiedQr?.errCode)", for: .normal)
                btnStep2.setTitleColor(UIColor.systemRed, for: .normal)
                showNoticeView(title: "mainVCActionFailed".localized, contents: "mainIncorrectQRCodeDetect".localized + " \(verifiedQr?.errCode)")
            }
        }
        else{
            isAbleToMeasure.step2 = false
            btnStep2.setTitle("mainVCActionNeeded".localized, for: .normal)
            btnStep2.setTitleColor(UIColor.systemRed, for: .normal)
        }
        
        if isAbleToMeasure.step3{
            btnStep3.setTitle("mainVCActionCompleted".localized, for: .normal)
            btnStep3.setTitleColor(UIColor.systemBlue, for: .normal)
        }
        else {
            btnStep3.setTitle("mainVCActionNeeded".localized, for: .normal)
            btnStep3.setTitleColor(UIColor.systemRed, for: .normal)
        }
        
        MeasuringManager.shared.isLogRecording = false
    }
    
    func checkCameraAccess() {
        switch cameraAcessStatus {
        case .notDetermined:
            let cameraMediaType = AVMediaType.video
            AVCaptureDevice.requestAccess(for: cameraMediaType) { granted in
                DispatchQueue.main.async { [self] in
                    if granted {
                        labelCameraAccess.text = "mainVCCameraAccessComplete".localized
                        checkSystemVersion()
                    } else {
                        labelCameraAccess.text = "mainVCCameraAccessFailed".localized
                        checkSystemVersion()
                        disabledAllSet()
                    }
                }
            }
            break
        case .restricted, .denied:
            labelCameraAccess.text = "mainVCCameraAccessFailed".localized
            disabledAllSet()
            break
        case .authorized:
            labelCameraAccess.text = "mainVCCameraAccessComplete".localized
            break
        }
    }
    
    func checkSystemVersion() {
        guard let systemVersion = Int(UIDevice.current.systemVersion.split(separator: ".")[0]) else {
            labelOsVersion.text = "mainVCOSVersionNotConfirm".localized
            disabledAllSet()
            return
        }
        if systemVersion >= 13 {
            labelOsVersion.text = "mainVCOSVersionConfirm".localized
        }
        else {
            labelOsVersion.text = "mainVCOSVersionLow".localized
            disabledAllSet()
        }
    }
    
    @IBAction func btnActionCheckIsAbleToMeasure(_ sender: UIButton) {
/// ------------------------------------------------------------------------------------------
    /// framework1drop - Function
    /// ------------------------------------------------------------------------------------------
    /// Declaration - func isAvailableMeasurement(deviceName:String)
    /// ReturnType - (hasError: String, errCode: String)
    /// Discussion - This checks the compatibility of the device with the app.
    ///     This method returns the device code(e.g:  "iPhone12,1") as the parameter.
    ///     If ReturnType of hasError is false, continue to measure.
    ///     If hasError is true, please refer to the Error Codes Table to identify the returned errCode.
    /// Attention/Tip - This function is required during the measuring process. Please be mindful when using.
    /// ------------------------------------------------------------------------------------------
    /// Link - https://www.notion.so/isAvailableMeasurement-deviceName-String-2dad00284e2d4340815bbaa2e333b8d4
/// ------------------------------------------------------------------------------------------
        let possibieMeasure = availableMeasure.isAvailableMeasurement()
        if !possibieMeasure.hasError {
            isAbleToMeasure.step1 = true
            btnStep1.setTitle("mainVCActionCompleted".localized, for: .normal)
            btnStep1.setTitleColor(UIColor.systemBlue, for: .normal)
            showNoticeView(title: "mainVCActionCompleted".localized, contents: "mainVCDeviceIsCanMeasure".localized)
        }
        else{
            isAbleToMeasure.step1 = false
            btnStep1.setTitle("mainVCFailed".localized + "(\(possibieMeasure.errCode)", for: .normal)
            btnStep1.setTitleColor(UIColor.systemRed, for: .normal)
            showNoticeView(title: "mainVCActionFailed".localized, contents: "mainVCDeviceIsCanNotMeasure".localized + " \(possibieMeasure.errCode)")
        }
    }
    @IBAction func btnActionQrStringSet(_ sender: UIButton) {
        if !isAbleToMeasure.step1 {
            showNoticeView(title: "mainVCActionFailed".localized, contents: "mainVCMakeSureToCompleteSteps".localized)
            return
        }
        DispatchQueue.main.async { [self] in
            performSegue(withIdentifier: MainToQrReaderSegue, sender: self)
        }
    }
    @IBAction func btnActionMeasure(_ sender: UIButton) {
        if !isAbleToMeasure.step1 || !isAbleToMeasure.step2{
            showNoticeView(title: "mainVCActionFailed".localized, contents: "mainVCMakeSureToCompleteSteps".localized)
            return
        }
        startMeasure()
    }
/// ------------------------------------------------------------------------------------------
    /// framework1drop - Function
    /// ------------------------------------------------------------------------------------------
    /// Declaration - func startMeasure(handler: @escaping (MeasuringState) -> Void)
    /// ReturnType - _
    /// Discussion - This function is responsible for the first half of the measuring process.
    ///     After verifying compatibility of device and QR code, this function is run.
    ///     This method returns the Callback function handler as the parameter.
    ///     state(MeasuringState type) in CallBack function is changed depending on the steps of the measuring process.
    ///     If you need access to each state, switch is recommended.
    /// Attention/Tip - This is the vital function that carrys out the measuring process. See link below prior to use.
    /// ------------------------------------------------------------------------------------------
    /// Link - https://www.notion.so/startMeasure-685721e248534e0da564c7f4ed44c23e
/// ------------------------------------------------------------------------------------------
    func startMeasure(){
        MeasuringManager.shared.startMeasure(handler: { state in
            switch state {
            case .PUT_CARTRIDGE:
                DispatchQueue.main.async { [self] in
                    performSegue(withIdentifier: MainToMeasureSegue, sender: self)
                    if let nextVC = self.presentedViewController as? MeasureViewController {
                        nextVC.changeLayout(step: "PUT_CARTRIDGE", explain: "mainVCPUTCARTRIDGEExplain".localized)
                    }
                    else if let nextToNextVC = self.presentedViewController as? QrReadStepViewController {
                        if let nextVC = nextToNextVC.presentedViewController as? MeasureStepViewController {
                            nextVC.changeLayout(step: "PUT_CARTRIDGE", explain: "mainVCPUTCARTRIDGEExplain".localized)
                        }
                    }
                }
                break
            case .PREPARE_MEASURE:
                break
            case .START_CALIBRATION:
                if let nextVC = self.presentedViewController as? MeasureViewController {
                    nextVC.changeLayout(step: "START_CALIBRATION", explain: "mainVCSTARTCALIBRATIONExplain".localized)
                }
                else if let nextToNextVC = self.presentedViewController as? QrReadStepViewController {
                    if let nextVC = nextToNextVC.presentedViewController as? MeasureStepViewController {
                        nextVC.changeLayout(step: "START_CALIBRATION", explain: "mainVCSTARTCALIBRATIONExplain".localized)
                    }
                }
                break
            case .WAIT_BLOOD_APPLY:
                if let nextVC = self.presentedViewController as? MeasureViewController {
                    nextVC.changeLayout(step: "WAIT_BLOOD_APPLY", explain: "mainVCWAITBLOODAPPLYExplain".localized)
                }
                else if let nextToNextVC = self.presentedViewController as? QrReadStepViewController {
                    if let nextVC = nextToNextVC.presentedViewController as? MeasureStepViewController {
                        nextVC.changeLayout(step: "WAIT_BLOOD_APPLY", explain: "mainVCWAITBLOODAPPLYExplain".localized)
                    }
                }
                break
            case .MEASURING_NOW:
                if let nextVC = self.presentedViewController as? MeasureViewController {
                    nextVC.changeLayout(step: "MEASURING_NOW", explain: "mainVCMEASURINGNOWExplain".localized)
                }
                else if let nextToNextVC = self.presentedViewController as? QrReadStepViewController {
                    if let nextVC = nextToNextVC.presentedViewController as? MeasureStepViewController {
                        nextVC.changeLayout(step: "MEASURING_NOW", explain: "mainVCMEASURINGNOWExplain".localized)
                    }
                }
                break
            case .STOP_MEASURE:
                break
            case .SHOW_RESULT(let result):
                if let nextVC = self.presentedViewController as? MeasureViewController {
                    nextVC.dismiss(animated: true, completion: {
                        self.qrStringValue = ""
                        self.isAbleToMeasure = (false, false, false)
                        self.reloadState()
                    })
                }
                else if let nextToNextVC = self.presentedViewController as? QrReadStepViewController {
                    if let nextVC = nextToNextVC.presentedViewController as? MeasureStepViewController {
                        nextVC.dismiss(animated: true, completion: {})
                    }
                    nextToNextVC.dismiss(animated: true, completion: {
                        self.qrStringValue = ""
                        self.isAbleToMeasure = (false, false, false)
                        self.reloadState()
                    })
                }
                self.showNoticeView(title: "mainVCMeasuringCompleteTitle".localized, contents: "mainVCMeasuringCompleteContents".localized + "\(result)")
                break
            case .ABORT_MEASURE(let err):
                if let nextVC = self.presentedViewController as? MeasureViewController {
                    nextVC.dismiss(animated: true, completion: {
                        self.isAbleToMeasure = (false, false, false)
                        self.qrStringValue = ""
                        self.reloadState()
                    })
                }
                else if let nextToNextVC = self.presentedViewController as? QrReadStepViewController {
                    if let nextVC = nextToNextVC.presentedViewController as? MeasureStepViewController {
                        nextVC.dismiss(animated: true, completion: {
                            nextToNextVC.dismiss(animated: true, completion: {
                                self.qrStringValue = ""
                                self.isAbleToMeasure = (false, false, false)
                                self.reloadState()
                            })}
                        )
                    }
                }
                self.showNoticeView(title: "mainVCMeasuringWithErrorTitle".localized, contents: "mainVCMeasuringWithErrorContents".localized + " \(err)")
                break
            }
        })
    }
    
    @IBAction func btnActionGoToMeasure(_ sender: UIButton) {
/// ------------------------------------------------------------------------------------------
    /// framework1drop - Function
    /// ------------------------------------------------------------------------------------------
    /// Declaration - func isAvailableMeasurement(deviceName:String)
    /// ReturnType - (hasError: String, errCode: String)
    /// Discussion - This checks the compatibility of the device with the app.
    ///     This method returns the device code(e.g:  "iPhone12,1") as the parameter.
    ///     If ReturnType of hasError is false, continue to measure.
    ///     If hasError is true, please refer to the Error Codes Table to identify the returned errCode.
    /// Attention/Tip - This function has to be called during the measuring process. Please be mindful when using.
    /// ------------------------------------------------------------------------------------------
    /// Link - https://www.notion.so/isAvailableMeasurement-deviceName-String-2dad00284e2d4340815bbaa2e333b8d4
/// ------------------------------------------------------------------------------------------
        let deviceStatus = availableMeasure.isAvailableMeasurement()
        if deviceStatus.hasError {
            showNoticeView(title: "mainVCErrorTitle".localized, contents: "mainVCErrorContentsCannotMeasureDevice".localized + "  \(deviceStatus.errCode)")
            return
        }
        DispatchQueue.main.async { [self] in
            performSegue(withIdentifier: MainToQrReadStepSegue, sender: self)
        }
    }
    
    func disabledAllSet(){
        btnStep1.isEnabled = false
        btnStep2.isEnabled = false
        btnStep3.isEnabled = false
        
        optionTableView.isHidden = true
        btnMeasure.isEnabled = false
    }
    
    func showNoticeView(title : String, contents : String, isCustom : Bool = false, cell : OptionTableViewCell? = nil, btnCount : Int = 1, folderName : String = ""){
        let noticeView = NoticeView()
        noticeView.setTitleContents(title: title, contents: contents)
        if isCustom{
            noticeView.setTwoButton()
            noticeView.txtFieldFolderString.isHidden = false
            noticeView.btnOkAction = {
                if noticeView.txtFieldFolderString.text == nil || noticeView.txtFieldFolderString.text == ""{
                    MeasuringManager.shared.isLogRecording = true
                    noticeView.hideDialog(superView: self)
                }
                else {
                    let customStatus = MeasuringManager.shared.setWriteLogAtCustomPath(fileDestination: self.createURL(folderName: noticeView.txtFieldFolderString.text!))
                    if customStatus.hasError{
                        noticeView.hideDialog(superView: self)
                        self.showNoticeView(title: "mainVCErrorTitle".localized, contents: "mainVCErrorContentsCustomFolderAccess".localized + " \(customStatus.errCode)" + "mainVCErrorContentsCustomFolderAccessAdd".localized, btnCount: 2, folderName: noticeView.txtFieldFolderString.text!)

                    }
                    else{
                        MeasuringManager.shared.isLogRecording = true
                        noticeView.hideDialog(superView: self)
                    }
                }
            }
//            noticeView.btnCancelAction = {
//                cell?.switchContentStatus.isOn = false
//                noticeView.hideDialog(superView: self)
//            }
        }
        else if btnCount > 1{
            noticeView.setTwoButton()
            noticeView.txtFieldFolderString.text = folderName
            noticeView.txtFieldFolderString.isEnabled = false
//            noticeView.btnCancelAction = {
//                MeasuringManager.shared.isLogRecording = false
//                cell?.switchContentStatus.isOn = false
//                noticeView.hideDialog(superView: self)
//            }
            
            noticeView.btnOkAction = {
                self.createFolder(folderName: folderName)
                MeasuringManager.shared.isLogRecording = true
                cell?.switchContentStatus.isOn = true
                noticeView.hideDialog(superView: self)
            }
        }
        else {
            noticeView.btnOkAction = {
                noticeView.hideDialog(superView: self)
            }
        }
        noticeView.showDialog(superView: self)
    }
    
    func createURL(folderName : String) -> URL {
        return FileManager.default.urls(for: .documentDirectory, in: .allDomainsMask)[0].appendingPathComponent(folderName)
    }
    
    func createFolder(folderName : String) {
        let docDir = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let dataPath = docDir + "/" + folderName + "/"
        
        if !FileManager.default.fileExists(atPath: dataPath) {
            do {
                try FileManager.default.createDirectory(atPath: dataPath, withIntermediateDirectories: false, attributes: nil)
            } catch {
                fatalError()
            }
        }
        
        let paths = FileManager.default.urls(for: .documentDirectory, in: .allDomainsMask)[0].appendingPathComponent(folderName)
        let test = MeasuringManager.shared.setWriteLogAtCustomPath(fileDestination: paths)
    }
    
    func registerForKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification , object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        guard let keyboardFrame = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }
        if optionViewValue.yToModify == 0{
            optionView.frame.origin.y -= keyboardFrame.height
            optionViewValue.yToModify = optionView.frame.origin.y
        }
        else {
            optionView.frame.origin.y = optionViewValue.yToModify
        }
        optionView.backgroundColor = UIColor.init(named: "color_Background")
    }

    @objc func keyboardWillHide(_ notification: Notification) {
        guard let keyboardFrame = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }
        optionView.frame.origin.y = optionViewValue.originY
        optionView.backgroundColor = UIColor.clear
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
}

extension MainViewController : UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return true
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 2
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return cellList[0].count
        }
        else if section == 1{
            return cellList[1].count
        }
        else{
            return 0
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = optionTableView.dequeueReusableCell(withIdentifier: "OptionCell", for: indexPath) as! OptionTableViewCell
        cell.labelContentName.text = cellList[indexPath.section][indexPath.item]
        if indexPath.section == 0{
            if indexPath.item == OptionSet.vgOnOff.rawValue {
                setCellSwitch(cell: cell)
                cell.switchContentStatus.isOn = MeasuringManager.shared.isEnabledGuideSound
                cell.actionSwitchContensStatus = {
                    MeasuringManager.shared.isEnabledGuideSound = cell.switchContentStatus.isOn
                }
            }
            else if indexPath.item == OptionSet.vgVolume.rawValue{
                setCellTxtField(cell: cell, cellIdx: indexPath.item)
            }
            else if indexPath.item == OptionSet.effectOnOff.rawValue {
                setCellSwitch(cell: cell)
                cell.switchContentStatus.isOn = MeasuringManager.shared.isEnabledEffectSound
                cell.actionSwitchContensStatus = {
                    MeasuringManager.shared.isEnabledEffectSound = Bool(cell.switchContentStatus.isOn)
                }
            }
            else if indexPath.item == OptionSet.effectVolume.rawValue{
                setCellTxtField(cell: cell, cellIdx: indexPath.item)
            }
        }
        if indexPath.section == 1{
            if indexPath.item == SupplySet.updateResource.rawValue{
                setCellBtn(cell : cell, cellIdx: indexPath.item)
            }
            else if indexPath.item == SupplySet.customVoiceGuidance.rawValue{
                cell.switchContentStatus.isOn = false
                setCellSwitch(cell: cell)
                cell.actionSwitchContensStatus = { [self] in
                    if cell.switchContentStatus.isOn{
                        let customVgView = CustomVoiceGuidanceView()
                        customVgView.labelTitle.text = "mainVCAudioGuidanceLabelTitle".localized
                        customVgView.setTxtFieldAndTxtViewAtTableView(customVgArr: strForCustomVg, langAndLocale: languageAndLocale)
                        customVgView.btnOkActionClosure = {
                            let strArrayAndLangValuesForCustom = customVgView.getTxtFieldAndTxtViewAtTableView()
                            let customStatus = MeasuringManager.shared.setCustomGuidance(customStringArray: strArrayAndLangValuesForCustom.customVgArr, langAndLocale: strArrayAndLangValuesForCustom.langAndLocale)
                            if !customStatus.hasError{
                                strForCustomVg.removeAll()
                                strForCustomVg = strArrayAndLangValuesForCustom.customVgArr
                                languageAndLocale = strArrayAndLangValuesForCustom.langAndLocale
                                showNoticeView(title: "mainVCSuccess".localized, contents: "mainVCAudioGuidanceCustomSuccess".localized)
                            }
                            else {
                                customVgView.hideDialog(superView: self)
                                showNoticeView(title: "mainVCErrorTitle".localized, contents: "mainVCAudioGuidanceCustomFailed".localized + " \(customStatus.errCode)")
                                cell.switchContentStatus.isOn = false
                            }
                            customVgView.hideDialog(superView: self)
                        }
                        customVgView.btnCancelActionClosure = {
                            customVgView.hideDialog(superView: self)
                            cell.switchContentStatus.isOn = false
                        }
                        customVgView.showDialog(superView: self)
                    }
                    else {
                        MeasuringManager.shared.setDefaultVoiceGuidance()
                    }
                }
            }
            else if indexPath.item == SupplySet.writeLog.rawValue{
                cell.switchContentStatus.isOn = false
                setCellSwitch(cell: cell)
                cell.actionSwitchContensStatus = {
                    if cell.switchContentStatus.isOn{
                        self.showNoticeView(title: "mainVCNoticeLogOn".localized,contents: "mainVCNoticeLogOnContents".localized, isCustom: true, cell : cell)
                    }
                    else {
                        self.showNoticeView(title: "mainVCNoticeLogOff".localized, contents: "mainVCNoticeLogOffContents".localized)
                    }
                }
                MeasuringManager.shared.isLogRecording = cell.switchContentStatus.isOn
            }
        }
        
        cell.txtFieldValue.delegate = self
        return cell
    }
    
    func setCellSwitch(cell : OptionTableViewCell){
        cell.switchContentStatus.isHidden = false
        cell.txtFieldValue.isHidden = true
        cell.btnChangeSet.isHidden = true
        
    }
    
    func setCellTxtField(cell : OptionTableViewCell, cellIdx : Int){
        cell.switchContentStatus.isHidden = true
        cell.txtFieldValue.isHidden = false
        cell.btnChangeSet.isHidden = true

        if cellIdx == OptionSet.vgVolume.rawValue{
            cell.txtFieldStatus = { [self] in
                guard let strValue = cell.txtFieldValue.text,
                      let floatValue = Float(strValue) else {
                    MeasuringManager.shared.guideSoundVolume = VgDefaultValue
                    showNoticeView(title: "mainVCErrorTitle".localized, contents: "mainVCNoticeInvalidSettings".localized)
                    optionTableView.reloadData()
                    return
                }
                if 0.1 > floatValue || floatValue > 1.0{
                    MeasuringManager.shared.guideSoundVolume = VgDefaultValue
                    showNoticeView(title: "mainVCErrorTitle".localized, contents: "mainVCNoticeInvalidSettings".localized)
                    optionTableView.reloadData()
                    return
                }
/// ------------------------------------------------------------------------------------------
    /// framework1drop - Variable
    /// ------------------------------------------------------------------------------------------
    /// Declaration - var guideSoundVolume: Float { get set }
    /// Discussion - You can control the volume of audio guidance and check settings.
    ///     Default value is 0.4, and the setting range is 0.1 ~ 1.0.
    ///     When measuring is completed, all values return to default values.
    /// Attention/Tip - Audio guidance may sound louder than the sound effects. Adjust volume as user sees fit.
    /// ------------------------------------------------------------------------------------------
    /// Link - https://www.notion.so/guideSoundVolume-d5aeb7753e3b444b8719c0e67fe75b18
/// ------------------------------------------------------------------------------------------
                MeasuringManager.shared.guideSoundVolume = floatValue
            }
            cell.txtFieldValue.text = String(MeasuringManager.shared.guideSoundVolume)
        }
        else if cellIdx == OptionSet.effectVolume.rawValue {
            cell.txtFieldStatus = { [self] in
                guard let strValue = cell.txtFieldValue.text,
                      let floatValue = Float(strValue) else {
                    MeasuringManager.shared.effectSoundVolume = EffectDefaultValue
                    showNoticeView(title: "mainVCErrorTitle".localized, contents: "mainVCNoticeInvalidSettings".localized)
                    optionTableView.reloadData()
                    return
                }
                if 0.1 > floatValue || floatValue > 1.0{
                    MeasuringManager.shared.effectSoundVolume = EffectDefaultValue
                    showNoticeView(title: "mainVCErrorTitle".localized, contents: "mainVCNoticeInvalidSettings".localized)
                    optionTableView.reloadData()
                    return
                }
/// ------------------------------------------------------------------------------------------
    /// framework1drop - Variable
    /// ------------------------------------------------------------------------------------------
    /// Declaration - var effectSoundVolume: Float { get set }
    /// Discussion - You can set the size of the sound effect that occurs during the measurement process, or you can obtain the setting status.
    ///     Default value is 1.0, and the setting range is 0.1 ~ 1.0.
    ///     When measuring is completed, all values return to default values.
    /// Attention/Tip - Temporarily adjust the instrument volume before measurement starts. When the measurement is finished, it is set to the original instrument volume size.
    /// ------------------------------------------------------------------------------------------
    /// Link - https://www.notion.so/effectSoundVolume-59baba512ce54b51a1fe8525291a49a0
/// ------------------------------------------------------------------------------------------
                MeasuringManager.shared.effectSoundVolume = floatValue
            }
            cell.txtFieldValue.text = String(MeasuringManager.shared.effectSoundVolume)
        }
    }
    
    func setCellBtn(cell : OptionTableViewCell, cellIdx : Int){
        cell.switchContentStatus.isHidden = true
        cell.txtFieldValue.isHidden = true
        cell.btnChangeSet.isHidden = false
        if cellIdx == SupplySet.updateResource.rawValue{
/// ------------------------------------------------------------------------------------------
    /// framework1drop - Function
    /// ------------------------------------------------------------------------------------------
    /// Declaration - func hasUpdate()
    /// ReturnType - (hasError: String, errCode: String)
    /// Discussion - Updates of files necessary for measuring are checked.
    ///     If ReturnType of hasError is false, there are files that need to be updated.
    ///     If hasError is true, please refer to the Error Codes Table to identify the returned errCode.
    /// Attention/Tip - This function is not required. However, regular verification is recommended for better results.
    /// ------------------------------------------------------------------------------------------
    /// Link - https://www.notion.so/hasUpdate-b8b63d8f384d4b8798cc06a266d92e17
/// ------------------------------------------------------------------------------------------
    /// framework1drop - Function
    /// ------------------------------------------------------------------------------------------
    /// Declaration - func updateResource()
    /// ReturnType - (hasError: String, errCode: String)
    /// Discussion - Files necessary for measuring are being updated.
    ///     If ReturnType of hasError is false, update was successful.
    ///     If hasError is true, please refer to the Error Codes Table to identify the returned errCode.
    /// Attention/Tip - This function is not required. However, regular verification is recommended for better results.
    /// ------------------------------------------------------------------------------------------
    /// Link - https://www.notion.so/updateResource-3a71ddf361364ba6bfe057cc3b44877d
/// ------------------------------------------------------------------------------------------
            let hasUpdataStatus = self.dataManager.hasUpdate()
            if hasUpdataStatus.hasError {
                cell.btnChangeSet.isEnabled = false
                cell.btnChangeSet.setTitle("mainVCErrorTitle".localized + " : " + hasUpdataStatus.errCode, for: .normal)
            }
            else {
                cell.btnChangeSet.isEnabled = true
                cell.btnChangeSet.setTitle("mainVCUpdateTitle".localized, for: .normal)
            }
            cell.actionBtnSetting = {
                let updateStatus = self.dataManager.updateResource()
                if !updateStatus.hasError{
                    self.showNoticeView(title: "mainVCUpdateSuccessTitle".localized, contents: "mainVCUpdateSuccessContents".localized)
                }
                else{
                    self.showNoticeView(title: "mainVCUpdateFailedTitle".localized, contents: "mainVCUpdateFailedContents".localized + " \(updateStatus.errCode)")
                }
            }
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        optionTableView.endEditing(true)
        return
    }
}

extension String {
    var localized: String {
        let tempString = NSLocalizedString(self, tableName: "Localizable", value: self, comment: "")
        if tempString == self {
            guard let path = Bundle.main.path(forResource: "en", ofType: "lproj") else {
                let basePath = Bundle.main.path(forResource: "Base", ofType: "lproj")!
                return Bundle(path: basePath)!.localizedString(forKey: self, value: "", table: nil)
            }
            return Bundle(path: path)!.localizedString(forKey: self, value: "", table: nil)
            
        } else {
            return tempString
        }
    }
}
