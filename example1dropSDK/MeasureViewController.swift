//
//  MeasureViewController.swift
//  example1dropSDK
//
//  Created by 허문용 on 2020/11/24.
//

import UIKit
import Framework1drop

class MeasureViewController: UIViewController {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contentsTitleLabel: UILabel!
    @IBOutlet weak var contentsTxtView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        contentsTxtView.isEditable = false
        // Do any additional setup after loading the view.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    func changeLayout(step : String, explain : String){
        titleLabel.text = "MeasureVCMeasuring".localized
        contentsTitleLabel.text = "MeasureVCNowStep".localized + step
        contentsTxtView.text = explain
    }
    @IBAction func btnActionCancel(_ sender: UIButton) {
        if let previousVC = self.presentingViewController as? MainViewController {
            self.dismiss(animated: true, completion: {
                MeasuringManager.shared.cancelMeasure(completion: {
                    previousVC.qrStringValue = ""
                    previousVC.initData()
                    previousVC.reloadState()
                    previousVC.optionTableView.reloadData()
                })
                previousVC.reloadState()
            })
        }
    }
}
