//
//  QrReaderViewController.swift
//  example1dropSDK
//
//  Created by 허문용 on 2020/11/23.
//

import UIKit
import AVFoundation

class QrReaderViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate {

    @IBOutlet weak var labelQrExplain: UILabel!
    @IBOutlet weak var cameraPreview: UIImageView!
    var previewLayer: AVCaptureVideoPreviewLayer!
    var captureSession: AVCaptureSession!
    var videoCaptureDevice: AVCaptureDevice!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        labelQrExplain.text = "QRReaderVCReadQR".localized
        captureSession = AVCaptureSession()
        DispatchQueue.main.async {
            let deviceDefault: AVCaptureDevice.DeviceType = .builtInDualCamera
            if let dualCameraDevice = AVCaptureDevice.default(deviceDefault, for: AVMediaType.video, position: .back) {
                self.videoCaptureDevice = dualCameraDevice
            } else if let backCameraDevice = AVCaptureDevice.default(.builtInWideAngleCamera, for: AVMediaType.video, position: .back) {
                self.videoCaptureDevice = backCameraDevice
            }
            
            if self.videoCaptureDevice.isFocusModeSupported(.continuousAutoFocus),
               self.videoCaptureDevice.isWhiteBalanceModeSupported(.continuousAutoWhiteBalance),
               self.videoCaptureDevice.isExposureModeSupported(.continuousAutoExposure) {
               do {
                    try self.videoCaptureDevice.lockForConfiguration()
                    self.videoCaptureDevice.focusMode = .continuousAutoFocus
                    self.videoCaptureDevice.whiteBalanceMode = .continuousAutoWhiteBalance
                    self.videoCaptureDevice.setExposureTargetBias(0.0, completionHandler: nil)
                    self.videoCaptureDevice.exposureMode = .continuousAutoExposure
                    self.videoCaptureDevice.unlockForConfiguration()
                
               } catch {
               }
            }
            else {
                do {
                     try self.videoCaptureDevice.lockForConfiguration()
                     self.videoCaptureDevice.whiteBalanceMode = .continuousAutoWhiteBalance
                     self.videoCaptureDevice.setExposureTargetBias(0.0, completionHandler: nil)
                     self.videoCaptureDevice.exposureMode = .continuousAutoExposure
                     self.videoCaptureDevice.unlockForConfiguration()
                 
                } catch {
                }
            }
        
        
            do {
                let videoInput = try AVCaptureDeviceInput(device: self.videoCaptureDevice)
                if (self.captureSession.canAddInput(videoInput)) {
                    self.captureSession.addInput(videoInput)
                } else {
                }
            } catch {

            }

            let metadataOutput = AVCaptureMetadataOutput()

            if (self.captureSession.canAddOutput(metadataOutput)) {
                self.captureSession.addOutput(metadataOutput)

                metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
                metadataOutput.metadataObjectTypes = [.qr]
                
            } else {
            }
        }
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.frame = cameraPreview.bounds
        previewLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        previewLayer.cornerRadius = 20
        previewLayer.masksToBounds = true
        
        cameraPreview.layer.addSublayer(previewLayer)
        cameraPreview.isHidden = false
        
        captureSession.startRunning()
    }
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {

        if let metadataObject = metadataObjects.first {
            guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
            guard let stringValue = readableObject.stringValue else { return }
            //AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            
            captureSession.stopRunning()
            if let previousVC = self.presentingViewController as? MainViewController {
                previousVC.qrStringValue = stringValue
                previousVC.reloadState()
                self.dismiss(animated: false, completion: {
                })
            }
        }
        
    }
    
    @IBAction func btnActionCancel(_ sender: UIButton) {
        captureSession.stopRunning()
        if let previousVC = self.presentingViewController as? MainViewController {
            self.dismiss(animated: false, completion: {
                previousVC.qrStringValue = ""
                previousVC.initData()
                previousVC.reloadState()
                previousVC.optionTableView.reloadData()
            })
        }
    }
    
}
