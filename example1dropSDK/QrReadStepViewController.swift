//
//  QrReadStepViewController.swift
//  example1dropSDK
//
//  Created by 허문용 on 2020/11/25.
//
import UIKit
import AVFoundation

class QrReadStepViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate {
    
    
    @IBOutlet weak var labelQrExplain2: UILabel!
    @IBOutlet weak var cameraPreview2: UIImageView!
    
    
    let QrReadStepToMeasureStep = "QrReadStepToMeasureStep"
    var previewLayer: AVCaptureVideoPreviewLayer!
    var captureSession: AVCaptureSession!
    var videoCaptureDevice: AVCaptureDevice!
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == QrReadStepToMeasureStep {
            let vcDestination = segue.destination
            vcDestination.modalPresentationStyle = .fullScreen
            vcDestination.modalTransitionStyle = .crossDissolve
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        labelQrExplain2.text = "QR을 읽으세요"
        captureSession = AVCaptureSession()
        DispatchQueue.main.async {
            let deviceDefault: AVCaptureDevice.DeviceType = .builtInDualCamera
            if let dualCameraDevice = AVCaptureDevice.default(deviceDefault, for: AVMediaType.video, position: .back) {
                self.videoCaptureDevice = dualCameraDevice
            } else if let backCameraDevice = AVCaptureDevice.default(.builtInWideAngleCamera, for: AVMediaType.video, position: .back) {
                self.videoCaptureDevice = backCameraDevice
            }
            
            if self.videoCaptureDevice.isFocusModeSupported(.continuousAutoFocus),
               self.videoCaptureDevice.isWhiteBalanceModeSupported(.continuousAutoWhiteBalance),
               self.videoCaptureDevice.isExposureModeSupported(.continuousAutoExposure) {
               do {
                    try self.videoCaptureDevice.lockForConfiguration()
                    self.videoCaptureDevice.focusMode = .continuousAutoFocus
                    self.videoCaptureDevice.whiteBalanceMode = .continuousAutoWhiteBalance
                    self.videoCaptureDevice.setExposureTargetBias(0.0, completionHandler: nil)
                    self.videoCaptureDevice.exposureMode = .continuousAutoExposure
                    self.videoCaptureDevice.unlockForConfiguration()
                
               } catch {
               }
            }
            else {
                do {
                     try self.videoCaptureDevice.lockForConfiguration()
                     self.videoCaptureDevice.whiteBalanceMode = .continuousAutoWhiteBalance
                     self.videoCaptureDevice.setExposureTargetBias(0.0, completionHandler: nil)
                     self.videoCaptureDevice.exposureMode = .continuousAutoExposure
                     self.videoCaptureDevice.unlockForConfiguration()
                 
                } catch {
                }
            }
        
        
            do {
                let videoInput = try AVCaptureDeviceInput(device: self.videoCaptureDevice)
                if (self.captureSession.canAddInput(videoInput)) {
                    self.captureSession.addInput(videoInput)
                } else {
                }
            } catch {

            }

            let metadataOutput = AVCaptureMetadataOutput()

            if (self.captureSession.canAddOutput(metadataOutput)) {
                self.captureSession.addOutput(metadataOutput)

                metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
                metadataOutput.metadataObjectTypes = [.qr]
                
            } else {
            }
        }
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.frame = cameraPreview2.bounds
        previewLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        previewLayer.cornerRadius = 20
        previewLayer.masksToBounds = true
        
        cameraPreview2.layer.addSublayer(previewLayer)
        cameraPreview2.isHidden = false
        
        captureSession.startRunning()
    }
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        if let metadataObject = metadataObjects.first {
            guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
            guard let stringValue = readableObject.stringValue else { return }
            //AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            
            captureSession.stopRunning()
            if let previousVC = self.presentingViewController as? MainViewController {
                let setQRCode = previousVC.dataManager.setStringQRCode(code: stringValue)
                if !setQRCode.hasError {
                    DispatchQueue.main.async { [self] in
                        performSegue(withIdentifier: QrReadStepToMeasureStep, sender: self)
                        previousVC.startMeasure()
                    }
                }
                else {
                    let noticeView = NoticeView()
                    noticeView.setTitleContents(title: "mainVCActionFailed".localized, contents: "mainIncorrectQRCodeDetect".localized + " \(setQRCode.errCode)")

                    noticeView.btnOkAction = {
                        noticeView.hideDialog(superView: self)
                        self.dismissQr2Main(previousVcParam: previousVC)
                    }
                    noticeView.showDialog(superView: self)
                    
                }
            }
        }
    }
    @IBAction func btnActionCancel(_ sender: UIButton) {
        captureSession.stopRunning()
        if let previousVC = self.presentingViewController as? MainViewController {
            dismissQr2Main(previousVcParam: previousVC)
        }
    }
    
    func dismissQr2Main(previousVcParam : MainViewController? = nil){
        var previousVC = previousVcParam
        if previousVC == nil{
            previousVC = self.presentingViewController as? MainViewController
        }
        DispatchQueue.main.async { [self] in
            self.dismiss(animated: false, completion: {
                previousVC?.qrStringValue = ""
                previousVC?.initData()
                previousVC?.reloadState()
                previousVC?.optionTableView.reloadData()
            })
        }
        
    }
}
