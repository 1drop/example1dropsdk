//
//  MeasureStepViewController.swift
//  example1dropSDK
//
//  Created by 허문용 on 2020/11/25.
//

import UIKit
import Framework1drop
class MeasureStepViewController: UIViewController {
    
    @IBOutlet weak var titleLabel2: UILabel!
    @IBOutlet weak var contentsTitleLabel2: UILabel!
    @IBOutlet weak var contentsTxtView2: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        contentsTxtView2.isEditable = false
        initLayout()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    func initLayout(){
        titleLabel2.text = "MeasureVCMeasuring".localized
        contentsTitleLabel2.text = "MeasureVCNowStep".localized + "PUT_CARTRIDGE"
        contentsTxtView2.text = "mainVCPUTCARTRIDGEExplain".localized
    }
    func changeLayout(step : String, explain : String){
        titleLabel2.text = "MeasureVCMeasuring".localized
        contentsTitleLabel2.text = "MeasureVCNowStep".localized + step
        contentsTxtView2.text = explain
    }
    @IBAction func btnActionCancel(_ sender: UIButton) {
        if let previousVc = self.presentingViewController as? QrReadStepViewController {
            if let morePreviousVc = previousVc.presentingViewController as? MainViewController{
                self.dismiss(animated: true, completion: {
                    previousVc.dismiss(animated: true, completion: {
                        MeasuringManager.shared.cancelMeasure(completion: {
                            morePreviousVc.qrStringValue = ""
                            morePreviousVc.initData()
                            morePreviousVc.reloadState()
                            morePreviousVc.optionTableView.reloadData()
                        })
                        
                    })
                })
            }
        }
    }
}
