//
//  ViewController.swift
//  example1dropSDK
//
//  Created by 허문용 on 2020/11/23.
//

import UIKit
import AVFoundation
import TestFramework

class MainViewController: UIViewController {
    
    @IBOutlet weak var labelCameraAccess: UILabel!
    @IBOutlet weak var labelOsVersion: UILabel!
    @IBOutlet weak var labelIsEnableToMeasure: UILabel!
    @IBOutlet weak var optionTableView: UITableView!
    
    let availableMeasure = AvailableMeasure()
    let optionList = ["음성안내", "효과음"]
    let dataManager = DataManager()
    var measuringManager: MeasuringManager?
    var cameraAcessStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
    var isAbleToMeasure : (camera : Bool, osVer : Bool, available : Bool) = (false, false, false)
    override func viewDidLoad() {
        super.viewDidLoad()
        initLayout()
        measuringManager = MeasuringManager.shared
    }
    func initLayout(){
        labelCameraAccess.text = "카메라 권한 획득 중"
        labelOsVersion.text = "OS version 확인 중"
        labelIsEnableToMeasure.text = "측정 가능 여부 확인 중"
        
        checkCameraAccess()
        if cameraAcessStatus != .notDetermined{
            checkSystemVersion()
            checkIsEnableToMeasure()
        }
        optionTableView.delegate = self
        optionTableView.dataSource = self
        let xibOptionTableView = UINib(nibName: "OptionTableViewCell", bundle: nil)
        optionTableView.register(xibOptionTableView, forCellReuseIdentifier: "OptionCell")
    }
    
    func checkCameraAccess() {
        switch cameraAcessStatus {
        case .notDetermined:
            let cameraMediaType = AVMediaType.video
            AVCaptureDevice.requestAccess(for: cameraMediaType) { granted in
                DispatchQueue.main.async { [self] in
                    if granted {
                        isAbleToMeasure.camera = granted
                        labelCameraAccess.text = "카메라 접근 권한 확인"
                        checkSystemVersion()
                        checkIsEnableToMeasure()
                    } else {
                        isAbleToMeasure.camera = granted
                        labelCameraAccess.text = "카메라 접근 권한 없음"
                        checkSystemVersion()
                        checkIsEnableToMeasure()
                    }
                }
            }
            break
        case .restricted, .denied:
            isAbleToMeasure.camera = false
            labelCameraAccess.text = "카메라 접근 권한 없음"
            break
        case .authorized:
            isAbleToMeasure.camera = true
            labelCameraAccess.text = "카메라 접근 권한 획득"
            break
        }
    }
    
    func checkSystemVersion() {
        guard let systemVersion = Int(UIDevice.current.systemVersion.split(separator: ".")[0]) else {
            print("\(UIDevice.current.systemVersion)")
            labelOsVersion.text = "iOS 버전 확인 실패"
            isAbleToMeasure.osVer = false
            return
        }
        if systemVersion >= 13 {
            isAbleToMeasure.osVer = true
            labelOsVersion.text = "iOS 최소 버전 충족"
        }
        else {
            isAbleToMeasure.osVer = false
            labelOsVersion.text = "iOS 최소 버전 미달"
        }
    }
    
    func checkIsEnableToMeasure(){
        let isEnable = availableMeasure.isAvailableMeasurement()
        isAbleToMeasure.available = !isEnable.hasError
        if isAbleToMeasure.camera && isAbleToMeasure.osVer && isAbleToMeasure.available {
            labelIsEnableToMeasure.text = "측정 가능"
        }
        else {
            labelIsEnableToMeasure.text = "측정 불가능 - \(isEnable.errorCode)"
        }
    }
}

extension MainViewController : UITableViewDelegate, UITableViewDataSource{
    // 테이블 뷰의 섹션 수를 정의한다. 여기서는 1개의 섹션만 이용한다.
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    // 테이블 뷰의 섹션 내부의 Cell 수를 정의한다. 여기서는 섹션 당 Cell이 data의 아이템 개수이다.
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return optionList.count
    }
    
    // Cell의 모양을 정의한다. 여기서는 CustomCell이라는 것을 연결하여 titleLabel을 꾸며준다.
    // 수정시 settingList 어레이 주의 할 것
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell { // indexPath.row는 현재 셀의 순서를 의미한다. 이 함수는 셀을 끝까지 다 돈다.
        let cell = optionTableView.dequeueReusableCell(withIdentifier: "OptionCell", for: indexPath) as! OptionTableViewCell
        cell.labelContentName.text = optionList[indexPath.row]
        if indexPath.row == 0 {
            cell.switchContentStatus.isOn = measuringManager?.isEnabledGuideSound ?? false
            cell.actionSwitchContensStatus = { [self] in
                measuringManager?.isEnabledGuideSound = cell.switchContentStatus.isOn
            }
        }
        else if indexPath.row == 1 {
            cell.switchContentStatus.isOn = measuringManager?.isEnabledEffectSound ?? false
            cell.actionSwitchContensStatus = { [self] in
                measuringManager?.isEnabledEffectSound = cell.switchContentStatus.isOn
            }
        }
        return cell // cell을 return 해야한다.
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
}

